<?php

//TODO: See if it can go in the FarmPEP module or stand alone?
//TODO: Maybe rename and add to the FarmPEP module package

namespace Drupal\farmpep_group_requests\EventSubscriber;

/** 
 * Based off the example in the Membership Request module README
 * https://git.drupalcode.org/project/grequest/-/blob/1.0.x/README.md 
 */


//We're 'hooking' into the Event Subscribe class
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
// Everything for messages
use Drupal\message\Entity\Message;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\farmpep_group_requests\EventSubscriber
 */
class FarmpepGroupEventsSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      'group_membership_request.create.pre_transition' => 'preCreate',
      'group_membership_request.create.post_transition' => 'postCreate',
      'group_membership_request.approve.pre_transition' => 'preApprove',
      'group_membership_request.approve.post_transition' => 'postApprove',
      'group_membership_request.reject.pre_transition' => 'preReject',
      'group_membership_request.reject.post_transition' => 'postReject',
    ];
  }

  public function preCreate(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
    // DEBUG
    // $account = \Drupal::currentUser();
    //   \Drupal::messenger()->addStatus('Pre Create: ' . $account->getDisplayName() . $account->getAccountName() . $account->id() . $account->getEmail() );
    //dpm($event);
  }

  public function postCreate(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
    // $account = \Drupal::currentUser();
    // \Drupal::messenger()->addStatus('Post Create: ' . $account->getDisplayName() . $account->getAccountName() . $account->id() . $account->getEmail() );
    $person_who_wants_to_join = $event->getEntity()->getOwner();
    //dpm($person_who_wants_to_join);
    $group = $event->getEntity()->getGroup();
    //dpm($group);
    $members = $group->getMembers();
    foreach ($members as $member) {
      $user = $member->getUser();
      $userids[] = $user->id();
    }
    //dpm($userids);
    foreach ($userids as $userid) {
      // Get user account details
      $account = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->load($userid);
      // Check to see if Email subscriptions are allowed - should return 0 or 1 if user is in the 
      // user__message_subscribe_email table
      // and null (maybe?) if user does not have an entry there
      $has_email_subscription = $account->message_subscribe_email->value;
      
      // Send message routine..
      if ($has_email_subscription) {

        // Call message notify service.
        $notifier = \Drupal::service('message_notify.sender');
        // Create a message with stewards as message creators. NB This just creates it. It doesn't send it.
        // $userid = $person_who_wants_to_join->id(); //DEBUG
        $message = Message::create(['template' => 'group_request_to_join', 'uid' => $userid]);
        //$message->set('field_node_ref', $entity);
        //NB the fields_* below all correspond to fields that have been added to the 'new_follow' template.
        $message->set('field_group_reference', $group);
        $message->set('field_user_reference', $person_who_wants_to_join);
        $message->save();

        // Check the users email digest settings to see if we should send immediately?
        // Get email digest value for this user - possible values are: null, "0", "message_digest:daily", "message_digest:weekly"
        $value = $account->message_digest->value;
        //dpm($value);
    
        if (isset($value)) {
          if ($value == "message_digest:weekly") {
            $notifier->send($message, [], 'message_digest:weekly');
          } elseif ($value == "message_digest:daily") {
            $notifier->send($message, [], 'message_digest:daily'); 
          } else {
            $notifier->send($message);
          }
        } else {
          $notifier->send($message);
        }
      }
    }
  }

  public function preApprove(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
  }

  public function postApprove(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
    // membership was approved by - send to other/all group stewards
  }

  public function preReject(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
  }

  public function postReject(WorkflowTransitionEvent $event) {
    //$this->setMessage($event, __FUNCTION__);
    // membership was rejected by - send to other/all group stewards
  }

  //Use $this->setMessage($event, __FUNCTION__); above to fire the below
  // Sends a message to the screen with useful debug info 
  protected function setMessage(WorkflowTransitionEvent $event, $phase) {
    //sends message like: Cooperative IT (grequest_status)- Pending at preCreate (workflow: request).
    $str = '@entity_label (@field_name)';
    $str .= '- @state_label at @phase (workflow: @workflow).';
    \Drupal::messenger()->addMessage(new TranslatableMarkup($str, [
      '@entity_label' => $event->getEntity()->getOwner()->id(),
      '@field_name' => $event->getFieldName(),
      '@state_label' => $event->getTransition()->getToState()->getLabel(),
      '@workflow' => $event->getWorkflow()->getId(),
      '@phase' => $phase,
    ]));
  }

}